import EmberObject from '@ember/object';
import Controller from '@ember/controller';
import { A } from '@ember/array';
import faker from 'faker';

export default Controller.extend({
  tableConfig: null,
  init() {
    this._super(...arguments);

    var object = EmberObject.extend({});

    var tableConfig = object.create();
    var tableRows = A([]);

    for (let i = 0; i < 100; i++) {
      var row = object.create();
      row.set("title", faker.lorem.sentence())
      var subtableData = A([]);
      for (let i = 0; i < Math.floor(Math.random() * 9)+1; i++) {
        var subRow = object.create();
        subRow.set("firstName", faker.name.firstName());
        subRow.set("lastName", faker.name.lastName());
        subRow.set("email", faker.internet.email());
        subtableData.push(subRow);
      }
      row.set("subtable", subtableData);
      row.set("expanded", false);
      tableRows.push(row);
    }

    tableConfig.set("rowData", tableRows);
    var icons = object.create();
    icons.set("openIcon", "chevron-down");
    icons.set("closedIcon", "chevron-right");
    icons.set("badgeIcon", "user")
    tableConfig.set("icons", icons);
    tableConfig.set("badgeTerm", "User");

    var subTableConfig = object.create();

    var subTableConfigColumns = A([]);
    var nameFieldConfig = object.create();
    var nameFieldFields = A(["lastName", "firstName"]);
    nameFieldConfig.set("fields", nameFieldFields);
    nameFieldConfig.set("label", "Name")
    subTableConfigColumns.push(nameFieldConfig);
    var emailFieldConfig = object.create();
    var emailFieldFields = A(["email"]);
    emailFieldConfig.set("fields", emailFieldFields);
    emailFieldConfig.set("label", "Email Address")
    subTableConfigColumns.push(emailFieldConfig);
    subTableConfig.set("columns", subTableConfigColumns);
    subTableConfig.set("sortFields", A(["lastName", "firstName"]))

    tableConfig.set("subTableConfig", subTableConfig)

    tableConfig.set("title", faker.lorem.sentence())

    this.set("tableConfig", tableConfig);

  }
});
