import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({

  allExpanded: computed('rowData.@each.expanded', function(){
    var collapsedRows = this.get('rowData').filter(function(item){
      return item.get('expanded') !== true;
    })
    return collapsedRows.length === 0 && !this.get('initialLoad');
  }),

  allCollapsed: computed('rowData.@each.expanded', function(){
    var expandedRows = this.get('rowData').filterBy('expanded', true);
    return expandedRows.length === 0;
  }),

  actions: {
    checkAll: function() {
      this.checkAll();
    },

    expandAll: function(){
      this.get('rowData').forEach(function(item){
        item.set('expanded', true);
      });
      // pass in action if additional funcaionality is needed
      if(this.get('expandAll') !== undefined){
        this.expandAll();
      }
    },

    collapseAll: function(){
      this.get('rowData').forEach(function(item){
        item.set('expanded', false);
      });
      // pass in action if additional funcaionality is needed
      if(this.get('collapseAll') !== undefined){
        this.collapseAll();
      }
    }
  }
});
