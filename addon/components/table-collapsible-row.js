import Component from '@ember/component';

export default Component.extend({
  tagName: 'tr',
  classNameBindings: ['row.expanded:collapsible-row-expanded:collapsible-row-collapsed'],
  actions: {
    clickRow() {
      this.set('row.expanded', !this.get('row.expanded'));
    }
  }
});
